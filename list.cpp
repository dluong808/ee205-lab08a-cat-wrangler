///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file list.cpp
/// @version 1.0
///
/// Singly linked list
///
/// @author @Daniel Luong<dluong@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm4 Cat Wrangler - EE 205 - Spr 2021
/// @date   @March 25 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cassert>

#include "list.hpp"

using namespace std;

// Checks if list is empty
const bool DoubleLinkedList::empty() const{

   // if head is null should be empty asserts that there cant be a tail if there is a head
   if (head == nullptr){
      assert( tail== nullptr);
      return true;
   }
   else return false;
}

// Newnode becomes the head and the old head is the next pointer
void DoubleLinkedList::push_front (Node* newNode){

   //If newNode is a pointer do nothing
   if (newNode == nullptr)
      return;

   if ( head != nullptr){
      newNode->prev = nullptr;
      newNode->next = head;
      head->prev = newNode;
      head=newNode;

   if( head == tail){// When only one elemnt
      newNode->prev=nullptr;
      newNode->next=tail;
      tail->prev=newNode;
      head=newNode;
   }
   } else { //Special case when list is empty
      newNode->next=nullptr;
      newNode->prev=nullptr;
      head=newNode;
      tail=newNode;
   }
   count++;

}

void DoubleLinkedList::push_back (Node* newNode){

   // if newnode is a nullptr we do nothing
   if( newNode == nullptr)
      return;

   if( tail != nullptr) {
      newNode->next = nullptr;
      newNode->prev = tail;
      tail->next = newNode;
      tail=newNode;

   if( tail == head){ //One Element
      newNode->prev=head;
      newNode->next=nullptr;
      head->next=newNode;
      tail=newNode;
   }
   }  else{ //special case when list is empty
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head=newNode;
      tail=newNode;
   }
   count ++;

}


//Saves old head pointer to a temp to return to main and iterates second element to become head
Node* DoubleLinkedList::pop_front(){
   
   if(head == nullptr)
      return nullptr;
   
   if(count == 2){
      tempNode=head;
      head=tail;
      head->next=nullptr;
      tail->prev=nullptr;
      count--;
      return tempNode;
   }
   
    if(head==tail){
         tempNode = head;
         head=nullptr;
         tail=nullptr;
    }
      else{
   tempNode = head;
   head=head->next;
   head->prev=nullptr;
   }
      count--;
      return tempNode;
   }

//Saves old tail pointer to a temp to return to main and iterates tail to be the prev pointer
Node* DoubleLinkedList::pop_back(){
   if(tail == nullptr)
      return nullptr;

   if(count == 2 ){
      tempNode=tail;
      tail=head;
      head->next=nullptr;
      tail->prev=nullptr;
      count--;
      return tempNode;
   }

    if(tail==head){ // Special case when there is only one element in list
         tempNode = tail;
      head=nullptr;
      tail=nullptr;
   }
   else{ // General Case for a pop back function
   tempNode = tail;
   tail=tail->prev;
   tail->next=nullptr;
   }

   count--;
   return tempNode;

}

//Returns the head pointer the first one
Node* DoubleLinkedList::get_first() const {
   return head;
}

//Returns the tail pointer 
Node* DoubleLinkedList::get_last() const {
   return tail;
}

// Returns the next node
Node* DoubleLinkedList::get_next( const Node* currentNode ) const{

   if (currentNode==nullptr)
      return nullptr;
   //If newNode is not in the list return
   if(currentNode != tail)
   return currentNode->next;
   else return nullptr;
   
}

Node* DoubleLinkedList::get_prev (const Node* currentNode) const{
   
   //If currentNode is not in the list return
   if (currentNode==nullptr)
      return nullptr;

   if (currentNode != head)
      return currentNode->prev;
   else return nullptr;
}

void DoubleLinkedList::insert_after (Node* currentNode, Node* newNode) {

if (currentNode==nullptr && head == nullptr){
	push_front (newNode);
	return;
}

if (currentNode!=nullptr && head == nullptr) {
	assert ( false );
}
if (currentNode ==nullptr && head != nullptr)  {
assert ( false );
}

assert( currentNode != nullptr && head !=nullptr);

assert( isIn( currentNode) );

assert( !isIn( newNode) );

if(count==1) {
   tail=newNode;
   tail->prev=head;
   head->next=tail;
   count++;
   return;
}

if(currentNode == tail){
   push_back(newNode);
   return;
}

tempNode=currentNode->next;
newNode->next=tempNode;
newNode->prev=currentNode;
tempNode->prev=newNode;
currentNode->next=newNode;
count++;
}

void DoubleLinkedList::insert_before (Node* currentNode, Node* newNode) {

if (currentNode==nullptr && tail  == nullptr){
	push_back (newNode);
	return;
}
if (currentNode!=nullptr && tail == nullptr) {
	assert ( false );
}

if (currentNode ==nullptr && tail != nullptr)  {
assert ( false );
}


assert( currentNode != nullptr && tail !=nullptr);

assert( isIn( currentNode) );

assert( !isIn( newNode) );

if(count==1){
   head=newNode;
   head->next=tail;
   tail->prev=head;
   count++;
   return;
}

if(currentNode==head){
   push_front(newNode);
   return;
}

tempNode=currentNode->prev;

newNode->next=currentNode;
newNode->prev=tempNode;
tempNode->next=newNode;
currentNode->prev=newNode;
count++;
}

void DoubleLinkedList::swap ( Node* node1, Node* node2) {
   //Make Sure both nodes are not nullptrs and list isnt just 1 element
   if (count <= 1)
      return;
   if( node1 == nullptr || node2 ==  nullptr)
      return;
   if( node1 == node2)
      return;
   //Make sure Both nodes are in list
   
   assert( isIn(node1) );
   assert( isIn(node2) );

   if(count == 2){
   tempNode=head;
   head=tail;
   tail=tempNode;
   head->next=tail;
   tail->prev=head;
   head->prev = tail->next = nullptr;
   }

 if(count >= 3){//All cases of 3 node or more

      Node* temp1prev = nullptr;
      Node* temp1next = nullptr;

      Node* temp2prev = nullptr;
      Node* temp2next = nullptr;
   
     if( (node1 == head) || (node2==head) ) {
        
     if(node1 == head){ //Special cases where node1 == head

      if(node2 == tail){ // node2 == tail
         
         //Save head next and tail prev to temp nodes
         temp1next = node1->next;
         temp2prev = node2->prev;
         
         //Swap head and tail
         tempNode=head;
         head=tail;
         tail=tempNode;

         head->prev = nullptr;
         head->next = temp1next;
         temp1next->prev = head;
         
         tail->next = nullptr;
         tail->prev = temp2prev;
         temp2prev->next = tail;
         
         }
      else if(node1->next==node2){// Adjacent Case
         temp2next = node2->next;

         //Swap head and node2 values
         tempNode=head;
         head=node2;
         node2=tempNode;
         
         // Make correct next and prev values 
         head->prev=nullptr;
         head->next=node2;
         node2->prev=head;
         node2->next=temp2next;
         temp2next->prev=node2;
      }
      else{// General cases with node1 == head
         temp1next = node1->next;
         temp2prev = node2->prev;
         temp2next = node2->next;

         tempNode=head;
         head=node2;
         node2=tempNode;

         head->prev = nullptr;
         head->next = temp1next;
         temp1next->prev = head;

         node2->prev = temp2prev;
         node2->next = temp2next;
         temp2prev->next = node2;
         temp2next->prev = node2;
         }
      return;
      }//end Node1 == head cases;
      
     if(node2==head){//Special cases where node2==head

         if(node1== tail){//Special case node1 is tail
         temp2next = node2->next;
         temp1prev = node1->prev;
         
         //Swap head and tail
         tempNode=head;
         head=node1;
         tail=tempNode;

         head->prev = nullptr;
         head->next = temp2next;
         temp2next->prev = head;

         tail->next = nullptr;
         tail->prev = temp1prev;
         temp1prev->next = tail;
         } // End node1 == tail special case
         
         else if(node2->next==node1){//Adjacent case
            temp1next = node1->next;

            tempNode=head;
            head=node1;
            node1=tempNode;

            head->prev = nullptr;
            head->next = node1;
            node1->prev = head;
            node1->next = temp1next;
            temp1next->prev = node1;
         }//End Adjacent case
         
         else{ // General case of when node2 == head
            temp2next = node2->next;
            temp1prev = node1->prev;
            temp1next = node1->next;

            tempNode=head;
            head=node1;
            node1=tempNode;

            head->prev = nullptr;
            head->next = temp2next;
            temp2next->prev = head;

            node1->prev=temp1prev;
            node1->next=temp1next;
            temp1prev->next = node1;
            temp1next->prev = node1;
            }
         return;
            }//End node2== head cases
      }// End Head cases
      
     
     if(node1 == tail || node2 == tail){ //Special tail cases Wont do Tail and Head cases as done above in head cases
         //cout << " Im node 1 or 2 is tail"<< endl;
        if(node1 == tail){ // Special cases where node1 == tail
            
            if(node2 == head){// Node1== head case
               temp1prev = node1->prev;
               temp2next = node2->next;

               tempNode=node2;
               head=node1;
               tail=tempNode;

               head->prev=nullptr;
               head->next=temp2next;
               temp2next->prev = head;
               tail->next=nullptr;
               tail->prev=temp1prev;
               temp1prev->next=tail;
            }


            else if(node1->prev==node2){//Adjacent case
              temp2prev = node2->prev;

              tempNode=tail;
              tail=node2;
              node2=tempNode;

              tail->next=nullptr;
              tail->prev=node2;
              node2->next=tail;
              node2->prev=temp2prev;
              temp2prev->next = node2;
           }// End Adjacent case

           else{//General case of node1==tail
              temp1prev=node1->prev;
              temp2prev=node2->prev;
              temp2next=node2->next;

              tempNode=node1;
              tail=node2;
              node2=tempNode;

              tail->next = nullptr;
              tail->prev = temp1prev;
              temp1prev->next=tail;
              node2->next=temp2next;
              node2->prev=temp2prev;
              temp2next->prev = node2 ;
              temp2prev->next = node2;
           }//End General Cases
           return;
      
        }// Eng node1 == tail cases

        if(node2 == tail){//Special cases where node2 == tail
            
            if(node1 == head){// Node1== head case
               temp2prev = node2->prev;
               temp1next = node1->next;

               tempNode=node1;
               head=node2;
               tail=tempNode;

               head->prev=nullptr;
               head->next=temp1next;
               temp1next->prev = head;
               tail->next=nullptr;
               tail->prev=temp2prev;
               temp2prev->next=tail;
            }

           else if(node2->prev==node1){//Special adjacent case

              temp1prev = node1->prev;

              tempNode=tail;
              tail=node1;
              node1=tempNode;

              tail->next = nullptr;
              tail->prev = node1;
              node1->next = tail;
              node1->prev = temp1prev;
              temp1prev->next = node1;
           }

           else{// General case where node2 == tail
              temp2prev=node2->prev;
              temp1prev=node1->prev;
              temp1next=node1->next;

              tempNode=node2;
              tail=node1;
              node1=tempNode;

              tail->next = nullptr;
              tail->prev = temp2prev;
              temp2prev->next = tail;
              node1->next = temp1next;
              node1->prev = temp1prev;
              temp1prev->next = node1;
              temp1next->prev = node1;
           }
           return;
        }//End node2 == tail cases
      }// End Cases where node1 or node2 == tail

     if( (node1->next==node2) || (node2->next==node1) ) { //General Adjacent cases with no heads or tails
     //    cout << " Im in adjacent" << endl;
        if(node1->next==node2){//Case where node1 is previous node
           temp2next = node2->next;
           temp1prev= node1->prev;
           
           node1->next=temp2next;
           temp2next->prev = node1;
           node1->prev=node2;
           node2->prev=temp1prev;
           temp1prev->next=node2;
           node2->next=node1;
           return;

        }

        if(node2->next == node1) {//Case where node2 is previous node
         temp1next = node1->next;
         temp2prev = node2->prev;

         node2->next=temp1next;
         temp1next->prev=node2;
         node2->prev=node1;
         node1->prev=temp2prev;
         temp2prev->next=node1;
         node1->next=node2;
         return;
        }
     }//End General Adjacent Cases
      
     //If no special cases then general swap
     temp1prev=node1->prev;
     temp1next=node1->next;
     temp2prev=node2->prev;
     temp2next=node2->next;

     tempNode=node1;
     node1=node2;
     node2=tempNode;

     node1->prev=temp1prev;
     node1->next=temp1next;
     temp1prev->next=node1;
     temp1next->prev=node1;

     node2->prev=temp2prev;
     node2->next=temp2next;
     temp2prev->next=node2;
     temp2next->prev=node2;


 }//End Count >= 3 Cases
}

//Checks is node passed through is in this list
const bool DoubleLinkedList::isIn (Node* aNode) const {
   Node* currentNode = head;

   while ( currentNode != nullptr) {
      if( aNode == currentNode )
         return true;
      currentNode = currentNode->next;
   }
   return false;
}

//Counts how many times it goes from node to node
unsigned int DoubleLinkedList::size() const{
   return count;
}

const bool DoubleLinkedList::isSorted() const {
   if( count<= 1)
      return true;

   for( Node* i =head; i->next != nullptr ; i =i->next){
      if ( *i > *i->next)
            return false;
            }
    return true;
}
 
void DoubleLinkedList::insertionSort() {

      if( count <= 1 )
      return;

   //Initializes currentNode to head
   for( Node* currentNode = head; currentNode != nullptr; currentNode=currentNode->next){
            
         // Initializes iterator the the next and will swap if its bigger if not temp will iterate through the list
         for(Node* iterator = currentNode->next; iterator!= nullptr; iterator=iterator->next){

            if( *currentNode > *iterator ){
         swap(currentNode,iterator);
         currentNode=iterator;
         iterator=currentNode;
            }
         }
   }
}
   
      


const bool DoubleLinkedList::validate() const {
   if(head==nullptr){
      assert( tail==nullptr);
      assert( count==0 );
   }else{
      assert( tail!=nullptr);
      assert( count!=0);
   }

   if( tail == nullptr) {
      assert( head == nullptr );
      assert( count ==0 );
   } else{
      assert( head != nullptr);
      assert( count != 0);
   }

   if( head != nullptr && tail == head ) {
      assert( count == 1);
   }

   unsigned int forwardCount = 0;
   Node* currentNode = head;
   // Count forward through the list
   while( currentNode != nullptr) {
      forwardCount++;
      if( currentNode->next != nullptr) {
         assert( currentNode->next->prev == currentNode );
      }
      currentNode = currentNode->next;
   }
   assert( count == forwardCount );

   // Count backward
   unsigned int backwardCount = 0;
   currentNode = tail;
   while( currentNode != nullptr) {
      backwardCount++;
      if( currentNode->prev != nullptr) {
         assert( currentNode->prev->next == currentNode );
      }
      currentNode = currentNode->prev;
   }
   assert( count==backwardCount );

   // List is valid
   //
   return true;

}

void DoubleLinkedList::dump() const{
   cout << "DoubleLinkedList:    head=[" << head << "]      tail=[" << tail << "]" << endl;
   for( Node* currentNode = head; currentNode != nullptr; currentNode = currentNode->next ) {
      cout << " ";
      currentNode->dump();
   }
}
