///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file list.hpp
/// @version 1.0
///
/// Singly linked list implementation
///
/// @author @Daniel Luong<dluong@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm4 C
//  Cat Wrangler - EE 205 - Spr 2021
/// @date   @April 8, 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"
#include "cat.hpp"

class DoubleLinkedList {
   protected:
      Node* head = nullptr;
      Node* tail = nullptr;
      Node* tempNode = nullptr;
      unsigned int count=0;
   public:
       const bool empty() const;
      const bool isIn( Node* aNode) const ;
      const bool validate() const;
      void dump() const;
      void insert_after (Node* currentNode, Node* newNode);
      void insert_before (Node* currentNode, Node* newNode);
      void push_front( Node* newNode );
      void push_back( Node* newNode );
      void swap(Node* node1, Node* node2);
      Node* pop_front();
      Node* pop_back();
      Node* get_first() const;
      Node* get_last() const;
      Node* get_next( const Node* currentNode ) const;
      Node *get_prev( const Node* currentNode ) const;
      unsigned int size() const;
      const bool isSorted() const;
      void insertionSort();
};


