///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file t.cpp
/// @version 2.0
///
/// Unit test for this lab
///
/// @author @Daniel Luong<dluong@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @April 8, 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cassert>

#include "cat.hpp"
#include "list.hpp"

using namespace std;


int main() {

   cout << "Cat Wrangler Unit Tests" << endl;

   Cat::initNames();

   DoubleLinkedList list = DoubleLinkedList();

   //assert( list.validate() );
   for( int i = 0 ; i < 10 ; i++ ) {

      list.push_front( Cat::makeCat() );

   // assert( list.validate() );
   }

   list.dump();


   for( int i = 0 ; i < 10 ; i++ ) {

      list.pop_front();

//    assert( list.validate() );
   }
         list.dump();
   

      for( Cat* cat = (Cat*)list.get_last() ; cat != nullptr ; cat = (Cat*)list.get_prev( cat )) {
      cout<< "Pop  front 10 cats"<< cat->getInfo() << endl;
   }
      cout << "Count" << list.size() << endl;

   for( int i = 0 ; i < 10 ; i++ ) {

      list.push_back( Cat::makeCat() );

//    assert( list.validate() );
   }
      list.dump();


   for( int i = 0 ; i <10 ; i++ ) {

      list.pop_back();

//    assert( list.validate() );
   }
      list.dump();

      for( Cat* cat = (Cat*)list.get_last() ; cat != nullptr ; cat = (Cat*)list.get_prev( cat )) {
      cout<< "Pop out 10 cats"<< cat->getInfo() << endl;
   }
      cout << "Count" << list.size() << endl;

      // Test swap function
   cout << "Test swap" << endl;

    Cat* cat1 = Cat::makeCat();
   Cat* cat2 = Cat::makeCat();
   Cat* cat3 = Cat::makeCat();
   Cat* cat4 = Cat::makeCat();



   

    for( Cat* cat = (Cat*)list.get_last() ; cat != nullptr ; cat = (Cat*)list.get_prev( cat )) {
      cout << cat->getInfo() << endl;
   }
   cout << "Add one item to list " <<endl;

   list.insert_after(list.get_last(),cat1);
   list.insert_after(list.get_last(),cat2);
   list.insert_after(list.get_last(),cat3);
   list.insert_after(list.get_last(),cat4);
   list.dump();

   cout<< "Finish insert after" << endl;
   list.pop_back();
   list.pop_back();
   list.pop_back();
   list.pop_back();
   
   list.insert_before(list.get_last(),cat1);
   list.insert_before(list.get_last(),cat2);
   list.insert_before(list.get_last(),cat3);
   list.insert_before(list.get_last(),cat4);

      
   list.dump();
    cout<< " Insertion Sort " << endl;
    list.insertionSort();
    list.dump();



}
/*
   
   cout << "Finish insert before" << endl;

   list.pop_front();
   list.pop_front();
   

   cout << " popped out 2 so 2 in list " << endl;
   list.dump();
   list.swap(list.get_first(),list.get_last());
   cout << " swapped 2" << endl;
   list.dump();



   cout << " pushed another onto list so 3" << endl;
   list.push_front( Cat::makeCat() );
   list.dump();

   cout << "Swapped head and tail " << endl;  
   list.swap(list.get_first(),list.get_last());
   list.dump();

    cout << "Swapped head and tail " << endl;
   list.swap(list.get_last(),list.get_first());
   list.dump();
   

   cout << " Swapped head and adjacent" << endl;
   list.swap(list.get_first(),list.get_next(list.get_first() ) );
   list.dump();

   cout << " Swapped head and adjacent" << endl;
   list.swap(list.get_next(list.get_first() ) ,list.get_first() ) ;
   list.dump();

   cout << " Swapped tail and adjacent" << endl;
   list.swap(list.get_next(list.get_first() ) ,list.get_last() ) ;
   list.dump();

   cout << " Swapped tail and adjacent" << endl;
   list.swap(list.get_last(),list.get_next(list.get_first() ) ) ;
   list.dump();

   

      cout << " Added one more 1 list so 4 cats" << endl;
   list.push_front( Cat::makeCat() );
   list.dump();

   cout << " Swapped head and 3rd element" << endl;
   list.swap(list.get_first(),list.get_next(list.get_next(list.get_first() ) ) );
   list.dump();

    cout << " tail and prev" << endl;
   list.swap(list.get_last(),list.get_prev(list.get_last()) );
   list.dump();

    cout << " tail and 2nd" << endl;
   list.swap(list.get_last(),list.get_next(list.get_first() ) );
   list.dump();

   cout << " tail and prev" << endl;
   list.swap(list.get_prev(list.get_last()),list.get_last() );
   list.dump();

    cout << " middle 2 " << endl;
   list.swap(list.get_next(list.get_first() ),list.get_prev(list.get_last()) );
   list.dump();
   
    list.push_front( Cat::makeCat() );
   cout << " 5 Cats " << endl;
   list.dump();
   
   cout << " tail and 3rd " << endl;
   list.swap(list.get_last(),list.get_next(list.get_next(list.get_first() ) ) );
   list.dump();

   cout<< "SortedE" << endl;
   list.insertionSort();
   list.dump();






   // One item in list
   list.push_front( Cat::makeCat() );
   list.insertionSort();
   list.dump();

}
/*

   list.swap( cat1, cat1 );
   list.dump();




   cout << "Add one item to list and swap" <<endl;

   // Two items in list
   list.push_back( cat2 );

   list.dump();




       for( Cat* cat = (Cat*)list.get_last() ; cat != nullptr ; cat = (Cat*)list.get_prev( cat )) {
      cout << cat->getInfo() << endl;
   }
   cout << "Add 2 item to list " <<endl;
   cout << " Count " << list.size() << endl;

   // list.dump();
   list.swap( list.get_first(), list.get_last() );

   list.dump();



       for( Cat* cat = (Cat*)list.get_last() ; cat != nullptr ; cat = (Cat*)list.get_prev( cat )) {
      cout << cat->getInfo() << endl;
   }
   cout << "2 Item swap" <<endl;
   cout << " Count " << list.size() << endl;

   

   // Three items in list
   list.push_back( cat3 );   // 1 2 3
       for( Cat* cat = (Cat*)list.get_last() ; cat != nullptr ; cat = (Cat*)list.get_prev( cat )) {
      cout << cat->getInfo() << endl;
   }

       list.dump();
   cout << "Add 3 item to list" <<endl;
   list.swap( list.get_first(), list.get_next( list.get_first() )) ; // 2 1 3


   cout<< "first swap" <<  endl;


   list.swap( list.get_first(), list.get_last() );  // 3 1 2
   cout<< "2nd swap" << endl;
   list.swap( list.get_next( list.get_first() ), list.get_last() );  // 3 2 1
       for( Cat* cat = (Cat*)list.get_last() ; cat != nullptr ; cat = (Cat*)list.get_prev( cat )) {
      cout << cat->getInfo() << endl;
   }
   cout << "Add 3 item to list and swap" <<endl;
   // Four items in list
   list.push_back( cat4 );  // 1 2 3 4

   cout << "Add 4 item to list" << endl;
   list.swap( list.get_first(), list.get_next( list.get_first() )) ;  // 2 1 3 4
   list.swap( list.get_prev( list.get_last() ), list.get_last() );    // 2 1 4 3
   list.swap( list.get_first(), list.get_last() );                    // 3 1 4 2
   list.swap( list.get_next( list.get_first() ), list.get_prev( list.get_last() )); // 3 4 1 2

}*/ 
